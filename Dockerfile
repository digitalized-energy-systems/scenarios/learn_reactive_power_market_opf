FROM tensorflow/tensorflow:1.15.5-gpu-py3

WORKDIR /workspace

RUN apt-get update -y
RUN apt install libgl1-mesa-glx -y
RUN apt-get install 'ffmpeg'\
    'libsm6'\
    'libxext6'  -y
RUN apt-get install ssh libopenmpi-dev python3-dev zlib1g-dev -y

RUN python3 -m pip install --upgrade pip

RUN pip3 install mpi4py
RUN pip3 install stable-baselines

COPY gym-powersystem gym-powersystem
RUN pip3 install -e gym-powersystem

COPY config config
COPY custom_callbacks.py custom_callbacks.py
COPY main.py main.py
COPY run.sh run.sh 

RUN find /workspace -type f -exec chmod 0755 {} \; \
	&& find /workspace -type d -exec chmod 0755 {} \;

ENV MPLCONFIGDIR=/tmp/
ENTRYPOINT ["/workspace/run.sh"]
