import csv
import os
from copy import deepcopy

import numpy as np
from stable_baselines.common.callbacks import BaseCallback


class SaveEpochCallback(BaseCallback):
    """
    Callback for saving a model every 'save_freq' steps.
    """

    def __init__(self, save_freq, total_timesteps, log_dir, verbose=0):
        super(SaveEpochCallback, self).__init__(verbose=verbose)
        self.save_freq = save_freq
        self.total_timesteps = total_timesteps
        self.log_dir = log_dir
        self.save_path = os.path.join(log_dir)

    def _init_callback(self):
        # Create folder if needed
        if self.log_dir is not None:
            os.makedirs(self.log_dir, exist_ok=True)

    def _on_step(self):
        if self.n_calls % self.save_freq == 0:
            epoch = int(self.num_timesteps / self.save_freq)
            print(f"Epoch: {epoch}")
            model_path = os.path.join(self.save_path, "model_{}".format(epoch))
            stats_path = os.path.join(
                self.save_path, "model_{}_stats.pkl".format(epoch)
            )
            buffer_path = os.path.join(
                self.save_path, "model_{}_buffer.pkl".format(epoch)
            )
            self.model.save(model_path)
            self.training_env.save(stats_path)
            if self.verbose > 0:
                print("Saving model to:", model_path)
                print("Saving stats to:", stats_path)


class TestCallback(BaseCallback):
    """
    Callback for testing a model every 'test_freq' steps.
    """

    def __init__(
        self,
        test_env,
        test_freq,
        n_test_episodes,
        network,
        log_dir,
        deterministic=True,
        verbose=0,
    ):
        super(TestCallback, self).__init__(verbose=verbose)
        self.test_env = test_env
        self.test_freq = test_freq
        self.n_test_episodes = n_test_episodes
        self.network = network
        self.log_dir = log_dir
        self.deterministic = deterministic
        self.best_train_deviation = np.inf
        self.best_test_deviation = np.inf

    def _init_callback(self):
        # Create folder if needed
        if self.log_dir is not None:
            os.makedirs(self.log_dir, exist_ok=True)

    def _sync_envs_normalization(self, training_env, test_env):
        test_env.obs_rms = deepcopy(training_env.obs_rms)
        test_env.ret_rms = deepcopy(training_env.ret_rms)

    def _evaluate_metrics(self, rl_rewards, opf_rewards):
        # create arrays
        rl_rew, opf_rew = np.array(rl_rewards), np.array(opf_rewards)
        # compute mpe
        deviation = np.mean((rl_rew / opf_rew - 1) * 100)
        # return reward mean, mpe
        return np.mean(rl_rew), deviation

    def _evaluate_policy(self, testing=False):
        """"""
        # setup environment for training/testing
        self.test_env.set_attr("testing", testing)
        # evaluate policy
        rl_rewards, opf_rewards = list(), list()
        obs = self.test_env.reset()
        for i in range(self.n_test_episodes):
            act, _ = self.model.predict(obs, deterministic=self.deterministic)
            # unpleasant unpacking ...
            [(_, _, rl_reward, is_conv, opf_reward, _)] = self.test_env.env_method(
                "eval_step", act.flatten()
            )
            if is_conv and rl_reward < opf_reward:
                rl_rewards.append(rl_reward)
                opf_rewards.append(opf_reward)
            obs = self.test_env.reset()

        return self._evaluate_metrics(rl_rewards, opf_rewards)

    def _on_step(self):
        if self.n_calls % self.test_freq == 0:
            print("Testing ... ")
            # Sync training and eval env
            self._sync_envs_normalization(self.training_env, self.test_env)
            self.test_env.training = False

            # evaluate model on training data
            train_rew, train_deviation = self._evaluate_policy(testing=False)
            # evaluate model on test data
            _, test_deviation = self._evaluate_policy(testing=True)

            # save deterministic training reward to csv
            with open(os.path.join(self.log_dir, "rew.csv"), "a") as f:
                csv_writer = csv.writer(f, delimiter=",")
                csv_writer.writerow([self.n_calls, train_rew])

            if self.verbose > 0:
                print(
                    f"Former best stats: \n"
                    f"train: {self.best_train_deviation:.2f} \n"
                    f"test: {self.best_test_deviation:.2f}"
                )
                print(
                    f"New stats: \n"
                    f"train: {train_deviation:.2f} \n"
                    f"test: {test_deviation:.2f}"
                )

            if test_deviation < self.best_test_deviation:
                if self.verbose > 0:
                    print("New best stats saved!")
                if self.log_dir is not None:
                    self.model.save(os.path.join(self.log_dir, "best_model"))
                    self.training_env.save(
                        os.path.join(self.log_dir, "best_model_stats.pkl")
                    )
                self.best_train_deviation = train_deviation
                self.best_test_deviation = test_deviation
