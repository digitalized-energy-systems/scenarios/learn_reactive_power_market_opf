#!/bin/bash

exec "$@" 2>&1 | tee /workspace/results/"$(date +'%FT%T').log"
