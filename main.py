import argparse
import os

import gym
import numpy as np
import tensorflow as tf
import yaml

from stable_baselines import SAC
from stable_baselines.sac.policies import MlpPolicy
from stable_baselines.common.vec_env import DummyVecEnv, VecNormalize
from stable_baselines.common.callbacks import CallbackList

import gym_powersystem
from gym_powersystem.core.utils import load_config
from custom_callbacks import SaveEpochCallback, TestCallback

# suppress tf warnings ...
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# setup argparse
parser = argparse.ArgumentParser()
parser.add_argument(
    "-s",
    "--seed",
    default=None,
    type=int,
    help="(int) random seed for this experiment",
)
parser.add_argument(
    "-n",
    "--network",
    default=None,
    type=str,
    help="(str) electric network for this experiment ('LV', 'LVM', 'MV', 'HV')",
)
args = vars(parser.parse_args())
seed = args["seed"]
network = args["network"]

# load network specific and shared config
config = load_config(__file__)
network_config = config[network]
shared_config = config["shared"]

# experiment name and logging directory
exp_name = "-".join([network, str(seed)])
tensorboard_dir = "./results/"
log_dir = tensorboard_dir + exp_name
os.makedirs(log_dir, exist_ok=True)

# create separate environments for training and testing
env_id = "PowerSystem-v0"
env = DummyVecEnv([lambda: gym.make(id=env_id, network=network)])
env = VecNormalize(env, norm_obs=True, norm_reward=True)
test_env = DummyVecEnv([lambda: gym.make(id=env_id, network=network)])
test_env = VecNormalize(test_env, norm_obs=True, norm_reward=True)

# sac agent
agent = SAC(
    policy=MlpPolicy,
    env=env,
    gamma=shared_config["gamma"],
    learning_rate=network_config["learning_rate"],
    buffer_size=network_config["buffer_size"],
    batch_size=shared_config["batch_size"],
    learning_starts=network_config["learning_starts"],
    ent_coef=shared_config["ent_coef"],
    verbose=shared_config["verbose"],
    tensorboard_log=log_dir,
    policy_kwargs=dict(layers=network_config["layers"]),
    seed=seed,
)

# callbacks
save_callback = SaveEpochCallback(
    save_freq=shared_config["save_freq"],
    total_timesteps=network_config["total_timesteps"],
    log_dir=log_dir,
    verbose=shared_config["verbose"],
)
test_callback = TestCallback(
    test_env=test_env,
    test_freq=shared_config["test_freq"],
    n_test_episodes=shared_config["n_test_episodes"],
    network=network,
    log_dir=log_dir,
    verbose=shared_config["verbose"],
)
callback = CallbackList([save_callback, test_callback])


# train the agent
agent.learn(
    total_timesteps=network_config["total_timesteps"],
    callback=callback,
    log_interval=shared_config["log_interval"],
    tb_log_name=exp_name,
)
