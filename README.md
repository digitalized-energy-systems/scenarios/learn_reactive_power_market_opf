# HowTo:

* **main.py** sets up and trains the agent  
* **config/config.yml** contains hyperparameters etc.  
* **custom_callbacks.py** provides functionality to periodically save and evaluate an agent during training  
* **gym-powersystem** is a python package that provides the environment model  
    * **gym-powersystem/gym_powersystem/envs/powersystem_env.py** implements the OpenAI gym interface to train agents in different electric grids  
    * **gym-powersystem/gym_powersystem/core/networks.py** specifies the electric grids (LV, HV, MV) and provides all functionality needed for the training process  

# Training:

* build with `docker build --tag <image-tag> .`
* run with `docker run -d -u $(id -u) --mount type=bind,source="$PWD"/results/,target=/workspace/results <image-tag> python /workspace/main.py --seed=0 --network=LV` specifying a seed and network type. 

