from setuptools import setup

setup(
    name="gym_powersystem",
    version="0.0.1",
    install_requires=[
        "gym",
        "matplotlib==3.3.3",
        "numba==0.52.0",
        "pandapower==2.2.2",
        "pyyaml",
        "simbench==1.2.0",
    ],
)
