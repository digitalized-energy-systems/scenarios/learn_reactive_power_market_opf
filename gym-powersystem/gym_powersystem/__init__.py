from gym.envs.registration import register

# register(
#     id="CigrePowerSystem-v0",
#     entry_point="gym_powersystem.envs:CigrePowerSystemEnv",
# )
#
# register(
#     id="LVSimbenchPowerSystem-v0",
#     entry_point="gym_powersystem.envs:LVSimbenchPowerSystemEnv",
# )
#
# register(
#     id="HVSimbenchPowerSystem-v0",
#     entry_point="gym_powersystem.envs:HVSimbenchPowerSystemEnv",
# )

register(
    id="PowerSystem-v0",
    entry_point="gym_powersystem.envs:PowerSystemEnv",
)
