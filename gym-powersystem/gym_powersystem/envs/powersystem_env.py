import os

import numpy as np
import pandas as pd

import gym
from gym import spaces
from gym_powersystem.core.networks import (
    LVNetwork,
    LVNetworkModified,
    MVNetwork,
    HVNetwork,
)


class PowerSystemEnv(gym.Env):
    metadata = {"render.modes": ["human"]}
    available_networks = {
        "LV": LVNetwork,
        "LVM": LVNetworkModified,
        "MV": MVNetwork,
        "HV": HVNetwork,
    }

    def __init__(self, network, testing=False):
        """
        Constructor of PowerSystemEnv. Setup electric network. Define observation and
        action space.
        """
        self.testing = testing

        # create network
        self.network = self.available_networks[network]()

        # define observation and action space
        self.observation_len = self.network.get_observation_len()
        self.observation_space = spaces.Box(
            low=-1, high=1, shape=(self.observation_len,), dtype=np.float32
        )

        self.action_len = self.network.get_action_len()
        self.action_space = spaces.Box(
            low=-1, high=1, shape=(self.action_len,), dtype=np.float32
        )

    def reset(self):
        """
        Reset the state of the environment and return an initial observation.
        """
        self.network.reset_network(testing=self.testing)
        return self.network.get_state()

    def step(self, action):
        """
        Take one step in the environment.
        """
        # calculate reactive power setpoints and run powerflow
        self.network.take_action(action)
        # get new obersation
        obs = self.network.get_state()
        # get reward
        reward, _, _ = self.network.get_reward()
        # done --> one step is one episode
        done = True
        return obs, reward, done, {}

    def render(self, mode="human"):
        """Currently not implemented."""
        if mode == "human":
            pass
        else:
            super(PowerSystemEnv, self).render(mode=mode)

    def eval_step(self, action):
        """
        Take one evaluation step in the environment.
        """
        # calculate reactive power setpoints and run powerflow
        self.network.take_action(action)
        # get reward
        rl_reward, penalty, violations = self.network.get_reward()
        valid_sol = False if penalty < 0 else True
        # run opf
        is_conv, opf_reward, opf_timer = self.network.optimal_powerflow()
        return valid_sol, violations, rl_reward, is_conv, opf_reward, opf_timer
