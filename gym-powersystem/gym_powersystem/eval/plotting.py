import os
import pickle

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import expon, lognorm
import simbench as sb


def reactive_power_plot(
    network, sgens, cost, q_rl, q_opf, q_max, save=False, path=None
):
    plt.close(fig="all")
    if network in ["LV", "LVM", "HV"]:
        reactive_power_plot_single(network, sgens, cost, q_rl, q_opf, q_max, save, path)
    elif network in ["MV"]:
        reactive_power_plot_multi(network, sgens, cost, q_rl, q_opf, q_max, save, path)


def reactive_power_plot_single(network, sgens, cost, q_rl, q_opf, q_max, save, path):
    """Reactive power plot to compare RL and IP solutions."""
    set_font_sizes()
    colors = ggplot_colors()
    fig, (ax1, ax2) = plt.subplots(
        nrows=2,
        ncols=1,
        sharex=True,
        gridspec_kw={"height_ratios": [1, 2]},
        figsize=(9, 5.5),
    )
    x = np.arange(len(sgens))
    if network == "HV":
        xlim = (x[0] - 1, x[-1] + 1)
        ax1.set_xlim(xlim)
        ax2.set_xlim(xlim)

    # plot cost on first axes
    ax1_width = 0.35 if network in ["LV", "LVM"] else 0.7
    ax1.bar(x=x, height=cost, width=ax1_width / 2, color=colors["gray"])
    ax1.set_ylabel(
        "Price Offer\nin €$\,/\,(\mathrm{kVarh})^2$"
        if network in ["LV", "LVM"]
        else "Price Offer\nin €$\,/\,(\mathrm{MVarh})^2$"
    )
    ax1.set_xticks([0.25, 0.50, 0.75, 1.00])

    # plot reactive power on second axes
    ax2_width = 0.25 if network in ["LV", "LVM"] else 0.35
    ax2.bar(
        x=x - ax2_width / 2,
        height=q_rl * 1000 if network == "LV" else q_rl,
        width=ax2_width,
        label="RL",
        color=colors["blue"],
    )
    ax2.bar(
        x=x + ax2_width / 2,
        height=q_opf * 1000 if network == "LV" else q_opf,
        width=ax2_width,
        label="IP",
        color=colors["green"],
    )

    ax2.axhline(color="black", lw=1)
    ax2.set_xticks(x)
    ax2.set_xticklabels(sgens, rotation=45)
    ax2.set_xlabel("Reactive Power Source")
    ax2.set_ylabel(
        "Reactive Power in kVar" if network == "LV" else "Reactive Power in MVar"
    )
    ax2.legend()
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "{}_example.pdf".format(network.lower())))
    plt.show()


def reactive_power_plot_multi(network, sgens, cost, q_rl, q_opf, q_max, save, path):
    set_font_sizes()
    MEDIUM_SIZE = 10
    plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc("ytick", labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc("legend", fontsize=MEDIUM_SIZE)  # legend fontsize
    colors = ggplot_colors()
    fig, (ax1, ax2, ax3, ax4, ax5, ax6) = plt.subplots(
        nrows=6,
        ncols=1,
        sharex=False,
        gridspec_kw={"height_ratios": [1, 3, 1, 3, 1, 3]},
        figsize=(10, 12),
    )
    # share x axes
    ax1.sharex(ax2)
    ax3.sharex(ax4)
    ax5.sharex(ax6)

    # split data into three parts
    onethird, twothirds = 45, 90
    x = np.arange(len(sgens))
    x1, x2, x3 = x[:onethird], x[onethird:twothirds], x[twothirds:]
    sgens1, sgens2, sgens3 = (
        sgens[:onethird],
        sgens[onethird:twothirds],
        sgens[twothirds:],
    )
    cost1, cost2, cost3 = cost[:onethird], cost[onethird:twothirds], cost[twothirds:]
    q_rl1, q_rl2, q_rl3 = q_rl[:onethird], q_rl[onethird:twothirds], q_rl[twothirds:]
    q_opf1, q_opf2, q_opf3 = (
        q_opf[:onethird],
        q_opf[onethird:twothirds],
        q_opf[twothirds:],
    )

    # x limits
    x1_lim = (x1[0] - 1, x1[-1] + 1)
    x2_lim = (x2[0] - 1, x2[-1] + 1)
    x3_lim = (x3[0] - 1, x3[-1] + 1)

    # y_lim = (-0.025, 0.15)
    y_lim = None

    # plot cost on first and third axes
    ax135_width = 0.75
    ax1.bar(x=x1, height=cost1, width=ax135_width / 2, color=colors["gray"])
    ax1.set_xticks([])
    ax1.set_xticklabels([])
    ax1.set_ylabel("Price Offer\nin €$\,/\,(\mathrm{MVarh})^2$")
    ax1.set_xlim(x1_lim)
    ax1.set_yticks([0.0, 0.50, 1.00])
    ax3.bar(x=x2, height=cost2, width=ax135_width / 2, color=colors["gray"])
    ax3.set_xticks([])
    ax3.set_xticklabels([])
    ax3.set_ylabel("Price Offer\nin €$\,/\,(\mathrm{MVarh})^2$")
    ax3.set_xlim(x2_lim)
    ax3.set_yticks([0.0, 0.50, 1.00])
    ax5.bar(x=x3, height=cost3, width=ax135_width / 2, color=colors["gray"])
    ax5.set_xticks([])
    ax5.set_xticklabels([])
    ax5.set_ylabel("Price Offer\nin €$\,/\,(\mathrm{MVarh})^2$")
    ax5.set_xlim(x3_lim)
    ax5.set_yticks([0.0, 0.50, 1.00])

    # plot reactive power on second and fourth axes
    ax246_width = 0.35
    ax2.bar(
        x=x1 - ax246_width / 2,
        height=q_rl1,
        width=ax246_width,
        label="RL",
        color=colors["blue"],
    )
    ax2.bar(
        x=x1 + ax246_width / 2,
        height=q_opf1,
        width=ax246_width,
        label="IP",
        color=colors["green"],
    )
    ax4.bar(
        x=x2 - ax246_width / 2,
        height=q_rl2,
        width=ax246_width,
        label="RL",
        color=colors["blue"],
    )
    ax4.bar(
        x=x2 + ax246_width / 2,
        height=q_opf2,
        width=ax246_width,
        label="IP",
        color=colors["green"],
    )
    ax6.bar(
        x=x3 - ax246_width / 2,
        height=q_rl3,
        width=ax246_width,
        label="RL",
        color=colors["blue"],
    )
    ax6.bar(
        x=x3 + ax246_width / 2,
        height=q_opf3,
        width=ax246_width,
        label="IP",
        color=colors["green"],
    )
    ax2.axhline(color="black", lw=1)
    ax4.axhline(color="black", lw=1)
    ax6.axhline(color="black", lw=1)
    ax2.set_xlim(x1_lim)
    ax4.set_xlim(x2_lim)
    ax6.set_xlim(x3_lim)
    ax2.set_xticks(x1)
    ax4.set_xticks(x2)
    ax6.set_xticks(x3)
    ax1.set_xticklabels(sgens1, rotation=45)
    ax2.set_xticklabels(sgens1, rotation=45)
    ax3.set_xticklabels(sgens2, rotation=45)
    ax4.set_xticklabels(sgens2, rotation=45)
    ax5.set_xticklabels(sgens3, rotation=45)
    ax6.set_xticklabels(sgens3, rotation=45)
    ax2.set_ylim(y_lim)
    ax4.set_ylim(y_lim)
    ax6.set_ylim(y_lim)
    ax2.set_yticks([-0.025, 0.0, 0.05, 0.10, 0.15])
    ax4.set_yticks([-0.025, 0.0, 0.05, 0.10, 0.15])
    ax6.set_yticks([-0.025, 0.0, 0.05, 0.10, 0.15])
    # ax2.set_yticks([-0.1, 0.0, 0.1, 0.2])
    # ax4.set_yticks([-0.1, 0.0, 0.1, 0.2])
    # ax6.set_yticks([-0.1, 0.0, 0.1, 0.2])

    # ax2.set_xlabel("Reactive Power Source")
    # ax4.set_xlabel("Reactive Power Source")
    ax6.set_xlabel("Reactive Power Source")
    ax2.set_ylabel("Reactive Power in MVar")
    ax4.set_ylabel("Reactive Power in Mvar")
    ax6.set_ylabel("Reactive Power in Mvar")
    ax6.legend()
    plt.tight_layout(pad=2, h_pad=0.5)
    if save:
        plt.savefig(os.path.join(path, "{}_example.pdf".format(network.lower())))
    plt.show()


def activations(save=False, path=None):
    """Plot neural network activation functions."""
    plt.style.use("seaborn-dark-palette")
    set_font_sizes()

    def sigmoid(z):
        return 1.0 / (1 + np.exp(-z))

    def tanh(z):
        return (np.exp(z) - np.exp(-z)) / (np.exp(z) + np.exp(-z))

    def relu(z):
        return np.maximum(0, z)

    def leaky_relu(z, alpha=0.15):
        return np.maximum(alpha * z, z)

    x = np.arange(-5, 5, 0.001)
    y1 = sigmoid(x)
    y2 = tanh(x)
    y3 = relu(x)
    y4 = leaky_relu(x)
    ys = [y1, y2, y3, y4]
    labels = ["Sigmoid", "Tanh", "ReLU", "Leaky ReLU"]

    fig, axes = plt.subplots(nrows=2, ncols=2)

    for i, ax in enumerate(np.array(axes).flatten()):
        ax.plot(x, ys[i], label=labels[i])
        # sns.lineplot(ax=ax, x=x, y=ys[i], label=labels[i])
        if i < 2:
            ax.set_xlim(-5, 5)
            ax.set_ylim(-1, 1)
        else:
            ax.set_xlim(-1, 1)
            ax.set_ylim(-1, 1)
        ax.grid()
        ax.set_title(labels[i])
        ax.set_xlabel("z")
        ax.set_ylabel("g (z)")

    # plt.subplots_adjust(wspace=0.5, hspace=0.5)
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "activations.pdf"))
    else:
        plt.show()


def capability_cost(save=False, path=None):
    """Plot assumed DER capability curve and quadratic cost function. """
    plt.style.use("seaborn-dark-palette")
    set_font_sizes()
    MEDIUM_SIZE = 10
    plt.rc("axes", titlesize=MEDIUM_SIZE)  # fontsize of the axes title
    plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels

    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))

    S = 1
    Q = np.arange(-1.0, 1.0, 0.00001)
    P = np.sqrt(S ** 2 - Q ** 2)

    ax1.plot(Q, P)
    ax1.axhline(color="black", lw=1)
    ax1.axvline(color="black", lw=1)
    ax1.set_xlabel("Reactive Power Q, p.u.")
    ax1.set_ylabel("Active Power P, p.u.")
    ax1.grid()
    ax1.set_title("I) Capability Curve")

    def arrange_y(x, cost):
        return cost * x ** 2

    x = np.arange(-1, 1, 0.0001)
    ys = list()
    labels = list()

    for i in range(4):
        cost = 0.25 + 0.25 * i
        ys.append(arrange_y(x, cost))
        labels.append(str(cost))

    for y in ys:
        ax2.plot(x, y, label=labels[i], linestyle="--")
        ax2.legend(
            [
                "   0.25 €",  # / MVarh$^2$",
                "   0.50 €",  # / MVarh$^2$",
                "   0.75 €",  # / MVarh$^2$",
                "   1.00 €",  # / MVarh$^2$",
            ]
        )
    ax2.axhline(color="black", lw=1)
    ax2.axvline(color="black", lw=1)
    ax2.grid()
    ax2.set_xlabel("Reactive Power Q, p.u.")
    ax2.set_ylabel("Cost in €")
    ax2.set_title("II) Quadratic Cost Function")
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "capability_cost.pdf"))
    else:
        plt.show()


def distributions(save=False, path=None):
    """Plots the distributions used for sampling."""
    # general setup
    set_font_sizes()
    SMALL_SIZE = 10
    plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("legend", fontsize=SMALL_SIZE)  # legend fontsize
    colors = ggplot_colors()
    rwidth = 0.95
    linewidth = 2.5
    alpha = 0.45
    hist_color = colors["green"]
    line_color = colors["blue"]

    # load data
    data = {}
    load_path = "./plots/distribution_data.npz"
    with np.load(load_path) as load_data:
        data = dict(load_data)

    # LV PV
    lv_pv_data = data["lv_pv"]
    params = expon.fit(lv_pv_data, floc=0)
    expon_dist = expon(0, 0.1)
    fig, ax = plt.subplots(figsize=(9, 5))
    x = np.linspace(0, 1, 1000)
    lv_bins = 8
    ax.hist(
        lv_pv_data,
        bins=lv_bins,
        density=True,
        rwidth=rwidth,
        alpha=alpha,
        color=hist_color,
        label="Avg. frequency of PV scale factors",
    )
    ax.plot(
        x,
        expon_dist.pdf(x),
        linewidth=linewidth,
        color=line_color,
        label="Exponential$\,(\\beta=0.15)$",
    )
    ax.legend()
    ax.set_xticks([i / 10 for i in range(11)])
    ax.set_xlabel("Scale Factor")
    ax.set_ylabel("Density")
    ax.grid()
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "lv_distributions.pdf"))
    else:
        plt.show()

    # HV PV
    hv_pv_data = data["hv_pv"]
    params = lognorm.fit(hv_pv_data)
    shape, loc, scale = params
    mu, sigma = np.log(scale), shape
    lognorm_dist = lognorm(*params)
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(9, 5))
    x = np.linspace(0, 1, 1000)
    hv_bins = 20
    ax1.hist(
        hv_pv_data,
        bins=hv_bins,
        density=True,
        rwidth=rwidth,
        alpha=alpha,
        color=hist_color,
        label="Avg. frequency of PV scale factors",
    )
    ax1.plot(
        x,
        lognorm_dist.pdf(x),
        linewidth=linewidth,
        color=line_color,
        label="Lognormal$\,(\mu={},\sigma={}$)".format(round(mu, 1), round(sigma, 1)),
    )
    ax1.legend()
    ax1.set_title("Scale factors of PV generating units")
    ax1.set_xticks([i / 10 for i in range(11)])
    ax1.set_xlabel("Scale Factor")
    ax1.set_ylabel("Density")
    ax1.grid()

    # HV WT
    hv_wt_data = data["hv_wt"]
    scale = 0.175
    params = expon.fit(hv_wt_data, fscale=scale)
    expon_dist = expon(*params)
    ax2.hist(
        hv_wt_data,
        bins=hv_bins,
        density=True,
        rwidth=rwidth,
        alpha=alpha,
        color=hist_color,
        label="Avg. frequency of WT scale factors",
    )
    ax2.plot(
        x,
        expon_dist.pdf(x),
        linewidth=linewidth,
        color=line_color,
        label="Exponential$\,(\\beta=0.175)$",
    )
    ax2.legend()
    ax2.set_title("Scale factors of WT generating units")
    ax2.set_xticks([i / 10 for i in range(11)])
    ax2.set_xlabel("Scale Factor")
    ax2.set_ylabel("Density")
    ax2.grid()
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "hv_distributions.pdf"))
    else:
        plt.show()

    # MV PV
    mv_pv_data = data["mv_pv"]
    params = expon.fit(mv_pv_data, floc=0)
    expon_dist = expon(*params)
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(9, 5))
    x = np.linspace(0, 1, 1000)
    mv_bins = 12
    ax1.hist(
        mv_pv_data,
        bins=mv_bins,
        density=True,
        rwidth=rwidth,
        alpha=alpha,
        color=hist_color,
        label="Avg. frequency of PV scale factors",
    )
    ax1.plot(
        x,
        expon_dist.pdf(x),
        linewidth=linewidth,
        color=line_color,
        label="Exponential$\,(\\beta=0.075)$",
    )
    ax1.legend()
    ax1.set_title("Scale factors of PV generating units")
    ax1.set_xticks([i / 10 for i in range(11)])
    ax1.set_xlabel("Scale Factor")
    ax1.set_ylabel("Density")
    ax1.grid()

    # MV Hydro
    mv_hydro_data = data["mv_hydro"]
    params = expon.fit(mv_hydro_data, floc=0)
    expon_dist = expon(*params)
    ax2.hist(
        mv_hydro_data,
        bins=mv_bins,
        density=True,
        rwidth=rwidth,
        alpha=alpha,
        color=hist_color,
        label="Frequency of pumped storage plant scale factors",
    )
    ax2.plot(
        x,
        expon_dist.pdf(x),
        linewidth=linewidth,
        color=line_color,
        label="Exponential$\,(\\beta=0.25)$",
    )
    ax2.legend()
    ax2.set_title("Scale factors of pumped storage plant")
    ax2.set_xticks([i / 10 for i in range(11)])
    ax2.set_xlabel("Scale Factor")
    ax2.set_ylabel("Density")
    ax2.grid()
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "mv_distributions.pdf"))
    else:
        plt.show()


def learning_curve(
    start_ep,
    end_ep,
    window,
    ax1_ylim,
    ax2_ylim,
    ax1_yticks,
    ax2_yticks,
    csv_path=None,
    network=None,
    save=False,
    path=None,
):
    """Plot learning curves for different electric networks."""
    set_font_sizes()
    colors = ggplot_colors()
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(9, 4))
    fig.subplots_adjust(hspace=0.05)

    df = pd.read_csv(csv_path, index_col=0)
    mean = df.loc[start_ep:end_ep, "mean"].rolling(window=window).mean()
    std = df.loc[start_ep:end_ep, "std"].rolling(window=window).mean()
    ep = mean.index
    # plot the same data on both axes
    alpha = 0.25
    linewidth = 2
    ax1.fill_between(
        ep,
        y1=mean + std,
        y2=mean - std,
        color=colors["blue"],
        alpha=alpha,
        label="Standard deviation",
    )
    ax1.plot(
        ep, mean, color=colors["blue"], linewidth=linewidth, label="Average return"
    )
    ax2.fill_between(
        ep,
        y1=mean + std,
        y2=mean - std,
        color=colors["blue"],
        alpha=alpha,
        label="Standard deviation",
    )
    ax2.plot(
        ep, mean, color=colors["blue"], linewidth=linewidth, label="Average return"
    )
    # zoom-in / limit the view to different portions of the data
    ax1.set_ylim(ax1_ylim)  # most of the data
    ax2.set_ylim(ax2_ylim)  # outliers
    # hide the spines between ax and ax2
    ax1.spines["bottom"].set_visible(False)
    ax2.spines["top"].set_visible(False)
    ax1.tick_params(axis="x", length=0)
    ax2.xaxis.tick_bottom()
    d = 0.5  # proportion of vertical to horizontal extent of the slanted line
    kwargs = dict(
        marker=[(-1, -d), (1, d)],
        markersize=12,
        linestyle="none",
        color="k",
        mec="k",
        mew=1,
        clip_on=False,
    )
    ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
    ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)
    ax1.set_yticks(ax1_yticks)
    ax2.set_yticks(ax2_yticks)
    ax2.ticklabel_format(axis="x", style="sci", scilimits=(0, 0))
    ax2.set_xlabel("Episode")
    ax2.set_ylabel("Reward")
    ax2.yaxis.set_label_coords(0.075, 0.5, transform=fig.transFigure)
    # ax1.set_title("{} Grid Learning Curve".format(network))
    ax1.legend()
    if save:
        plt.savefig(os.path.join(path, "{}_learning_curve.pdf".format(network.lower())))
    else:
        plt.show()


def learning_curve_args():
    learning_curve_args = {
        "LV": (
            0,
            800_000,
            1,
            (-7, -1),
            (-30, -8),
            [-1, -2, -3, -4, -5, -6],
            [-10, -15, -20, -25, -30, -35],
        ),
        "LVM": (
            0,
            800_000,
            1,
            (-7, -1),
            (-30, -8),
            [-1, -2, -3, -4, -5, -6],
            [-10, -15, -20, -25, -30, -35],
        ),
        "HV": (
            0,
            1_250_000,
            1,
            (-22, -5),
            (-140, -25),
            [-5, -7.5, -10, -12.5, -15, -17.5, -20],
            [-40, -60, -80, -100, -120, -140],
        ),
        "MV": (
            0,
            3_200_000,
            1,
            (-6, -0.5),
            (-40, -7.5),
            [-1, -2, -3, -4, -5],
            [-10, -20, -30, -40],
        ),
    }
    return learning_curve_args


def _metrics_to_df(metrics):
    df = pd.DataFrame.from_dict(
        {(int(i), j): metrics[i][j] for i in metrics.keys() for j in metrics[i].keys()},
        orient="index",
    )
    df.index.names = ("seed", "mode")
    df = df[
        [
            "mpe",
            "medpe",
            "ip_times",
            "rl_times",
            "speedup",
            "valid_conv",
            "valid_notconv",
            "invalid_conv",
            "invalid_notconv",
            "voltage_violations_invconv",
            "line_violations_invconv",
            "trafo_violations_invconv",
            "voltage_violations_invnotc",
            "line_violations_invnotc",
            "trafo_violations_invnotc",
        ]
    ]
    df.loc[:, "valid_conv":"invalid_notconv"] *= 100
    return df


def metrics_plot(pkl_path, network=None, save=False, path=None):
    if network in ["LV", "LVM", "HV"]:
        metrics_plot_single(pkl_path, network=network, save=save, path=path)
    elif network in ["MV"]:
        metrics_plot_multi(pkl_path, network=network, save=save, path=path)
    elif network in ["MVT"]:
        metrics_plot_testing(pkl_path, network=network, save=save, path=path)


def metrics_plot_single(pkl_path, network, save, path):
    """Plot accuracy metrics for LV, LVM, HV grids."""
    # prepare df
    with open(pkl_path, "rb") as file:
        metrics = pickle.load(file)
    df = _metrics_to_df(metrics)
    # create plot
    set_font_sizes()
    colors = ggplot_colors()
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(9, 6))
    width = 0.35
    x = np.arange(len(df.index.get_level_values("seed").unique()))
    training_data = df.xs("training", level="mode")["mpe"]
    test_data = df.xs("testing", level="mode")["mpe"]
    # bars
    ax.bar(
        x - width / 2,
        height=training_data,
        width=width,
        label="Training data",
        color=colors["blue"],
        zorder=3,
    )
    ax.bar(
        x + width / 2,
        height=test_data,
        width=width,
        label="Test data",
        color=colors["rose"],
        zorder=3,
    )
    # avg lines
    ax.axhline(
        training_data.mean(),
        label="Training data avg.",
        color=colors["blue"],
        linestyle="dashed",
        linewidth=3,
        alpha=0.6,
        zorder=2,
    )
    ax.axhline(
        test_data.mean(),
        label="Test data avg.",
        color=colors["rose"],
        linestyle="dashed",
        linewidth=3,
        zorder=2,
    )
    # std deviations
    ax.fill_between(
        np.arange(*ax.get_xbound(), 0.01),
        y1=training_data.mean() - training_data.std(),
        y2=training_data.mean() + training_data.std(),
        color=colors["blue"],
        alpha=0.15,
        label="Training data std. dev.",
        zorder=1,
    )
    ax.fill_between(
        np.arange(*ax.get_xbound(), 0.01),
        y1=test_data.mean() - test_data.std(),
        y2=test_data.mean() + test_data.std(),
        color=colors["rose"],
        alpha=0.25,
        label="Test data std. dev.",
        zorder=1,
    )
    ax.set_xlim([-0.75, 9.75])
    if network == "HV":
        ax.set_ylim([0, 15.25])
    ax.set_xticks(x)
    labels = ["Agent {}".format(i + 1) for i in np.nditer(x)]
    ax.set_xticklabels(labels)
    ax.set_xlabel("")
    ax.set_ylabel("Mean Percentage Error (MPE)")
    handles, labels = ax.get_legend_handles_labels()
    handles = [*handles[4:], *handles[:4]]
    labels = [*labels[4:], *labels[:4]]
    ax.legend(handles, labels)
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "{}_metrics.pdf".format(network.lower())))
    else:
        plt.show()


def metrics_plot_multi(pkl_path, network, save, path):
    """Plot accuracy metrics for MV grid."""
    # prepare df
    with open(pkl_path, "rb") as file:
        metrics = pickle.load(file)
    df = _metrics_to_df(metrics)
    # create plot
    set_font_sizes()
    colors = ggplot_colors()
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(9, 6))
    fig.subplots_adjust(hspace=0.05)
    width = 0.35
    x = np.arange(len(df.index.get_level_values("seed").unique()))
    training_data = df.xs("training", level="mode")["mpe"]
    test_data = df.xs("testing", level="mode")["mpe"]
    # ax1
    ax1.bar(
        x - width / 2,
        height=training_data,
        width=width,
        label="Training data",
        color=colors["blue"],
        zorder=3,
    )
    ax1.bar(
        x + width / 2,
        height=test_data,
        width=width,
        label="Test data",
        color=colors["rose"],
        zorder=3,
    )
    # avg lines
    ax1.axhline(
        training_data.mean(),
        label="Training data avg.",
        color=colors["blue"],
        linestyle="dashed",
        linewidth=3,
        alpha=0.6,
        zorder=2,
    )
    ax1.axhline(
        test_data.mean(),
        label="Test data avg.",
        color=colors["rose"],
        linestyle="dashed",
        linewidth=3,
        zorder=2,
    )
    # std deviations
    ax1.fill_between(
        np.arange(*ax1.get_xbound(), 0.01),
        y1=training_data.mean() - training_data.std(),
        y2=training_data.mean() + training_data.std(),
        color=colors["blue"],
        alpha=0.15,
        label="Training data std. dev.",
        zorder=1,
    )
    ax1.fill_between(
        np.arange(*ax1.get_xbound(), 0.01),
        y1=test_data.mean() - test_data.std(),
        y2=test_data.mean() + test_data.std(),
        color=colors["rose"],
        alpha=0.25,
        label="Test data std. dev.",
        zorder=1,
    )
    # ax2
    ax2.bar(
        x - width / 2,
        height=training_data,
        width=width,
        label="Training data",
        color=colors["blue"],
        zorder=3,
    )
    ax2.bar(
        x + width / 2,
        height=test_data,
        width=width,
        label="Test data",
        color=colors["rose"],
        zorder=3,
    )
    # avg lines
    ax2.axhline(
        training_data.mean(),
        label="Training data avg.",
        color=colors["blue"],
        linestyle="dashed",
        linewidth=3,
        alpha=0.6,
        zorder=2,
    )
    ax2.axhline(
        test_data.mean(),
        label="Test data avg.",
        color=colors["rose"],
        linestyle="dashed",
        linewidth=3,
        zorder=2,
    )
    # std deviations
    ax2.fill_between(
        np.arange(*ax2.get_xbound(), 0.01),
        y1=training_data.mean() - training_data.std(),
        y2=training_data.mean() + training_data.std(),
        color=colors["blue"],
        alpha=0.15,
        label="Training data std. dev.",
        zorder=1,
    )
    ax2.fill_between(
        np.arange(*ax2.get_xbound(), 0.01),
        y1=test_data.mean() - test_data.std(),
        y2=test_data.mean() + test_data.std(),
        color=colors["rose"],
        alpha=0.25,
        label="Test data std. dev.",
        zorder=1,
    )
    # hide the spines between ax and ax2
    ax1.spines["bottom"].set_visible(False)
    ax2.spines["top"].set_visible(False)
    ax1.tick_params(axis="x", length=0)
    ax2.xaxis.tick_bottom()
    d = 0.5  # proportion of vertical to horizontal extent of the slanted line
    kwargs = dict(
        marker=[(-1, -d), (1, d)],
        markersize=12,
        linestyle="none",
        color="k",
        mec="k",
        mew=1,
        clip_on=False,
    )
    ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
    ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)
    ax2.yaxis.set_label_coords(0.045, 0.5, transform=fig.transFigure)

    ax1.set_xlim([-0.75, 9.75])
    ax2.set_xlim([-0.75, 9.75])
    ax1.set_ylim(1000, 40000)
    ax2.set_ylim(0, 110)
    ax2.set_xticks(x)
    labels = ["Agent {}".format(i + 1) for i in np.nditer(x)]
    ax2.set_xticklabels(labels)
    # ax2.set_yticklabels([0, 20, 40, 60, 80, 100])
    ax2.set_xlabel("")
    ax2.set_ylabel("Mean Percentage Error (MPE)")
    handles, labels = ax1.get_legend_handles_labels()
    handles = [*handles[4:], *handles[:4]]
    labels = [*labels[4:], *labels[:4]]
    ax1.legend(handles, labels)
    plt.tight_layout(h_pad=0.05)
    if save:
        plt.savefig(os.path.join(path, "{}_metrics.pdf".format(network.lower())))
    else:
        plt.show()


def metrics_plot_testing(pkl_path, network, save, path):
    """Plot accuracy metrics for MVT grid."""
    # prepare df
    with open(pkl_path, "rb") as file:
        metrics = pickle.load(file)
    df = _metrics_to_df(metrics)
    # create plot
    set_font_sizes()
    colors = ggplot_colors()
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(9, 6))
    width = 0.35
    x = np.arange(len(df.index.get_level_values("seed").unique()))
    training_data = df.xs("training", level="mode")["mpe"]
    test_data = df.xs("testing", level="mode")["mpe"]
    # bars
    # ax.bar(
    #     x - width / 2,
    #     height=training_data,
    #     width=width,
    #     label="Training data",
    #     color=colors["blue"],
    #     zorder=3,
    # )
    ax.set_xlim([-0.75, 9.75])
    ax.bar(
        x + width / 2,
        height=test_data,
        width=width,
        label="Test data",
        color=colors["rose"],
        zorder=3,
    )
    # avg lines
    # ax.axhline(
    #     training_data.mean(),
    #     label="Training data avg.",
    #     color=colors["blue"],
    #     linestyle="dashed",
    #     linewidth=3,
    #     alpha=0.6,
    #     zorder=2,
    # )
    ax.axhline(
        test_data.mean(),
        label="Test data avg.",
        color=colors["rose"],
        linestyle="dashed",
        linewidth=3,
        zorder=2,
    )
    # std deviations
    # ax.fill_between(
    #     np.arange(*ax.get_xbound(), 0.01),
    #     y1=training_data.mean() - training_data.std(),
    #     y2=training_data.mean() + training_data.std(),
    #     color=colors["blue"],
    #     alpha=0.15,
    #     label="Training data std. dev.",
    #     zorder=1,
    # )
    ax.fill_between(
        np.arange(*ax.get_xbound(), 0.01),
        y1=test_data.mean() - test_data.std(),
        y2=test_data.mean() + test_data.std(),
        color=colors["rose"],
        alpha=0.25,
        label="Test data std. dev.",
        zorder=1,
    )
    ax.set_xticks(x)
    labels = ["Agent {}".format(i + 1) for i in np.nditer(x)]
    ax.set_xticklabels(labels)
    ax.set_xlabel("")
    ax.set_ylabel("Mean Percentage Error (MPE)")
    handles, labels = ax.get_legend_handles_labels()
    handles = [*handles[2:], *handles[:2]]
    labels = [*labels[2:], *labels[:2]]
    ax.legend(handles, labels)
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "{}_metrics.pdf".format(network.lower())))
    else:
        plt.show()


def violations(
    vio_idx,
    widths,
    positions,
    labels,
    xlim,
    ylim,
    pkl_path,
    network=None,
    save=False,
    path=None,
):
    """Plot violation boxplots for different electric networks."""
    # prepare df
    with open(pkl_path, "rb") as file:
        metrics = pickle.load(file)
    df = _metrics_to_df(metrics)
    violations = _get_violations(df)
    # create plot
    set_font_sizes()
    SMALL_SIZE = 10
    MEDIUM_SIZE = 11
    plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    colors = ggplot_colors()
    fig, ax = plt.subplots(figsize=(9, 3))
    bp = ax.boxplot(
        [violations[idx] for idx in vio_idx],
        vert=False,
        patch_artist=True,
        widths=widths,
        positions=positions,
        labels=labels,
    )
    for patch in bp["boxes"]:
        patch.set_linewidth(1.25)
        patch.set_facecolor(colors["blue"])
        patch.set_alpha(0.75)

    for whisker in bp["whiskers"]:
        whisker.set(color="k", linewidth=1.75, linestyle="-")
        whisker.set_alpha(0.75)

    for cap in bp["caps"]:
        cap.set(color="k", linewidth=1.5)

    for median in bp["medians"]:
        median.set(color="k", linewidth=1.25)
        median.set_alpha(0.75)

    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_xlabel("Maximum Violation in Percentage Points")
    plt.tight_layout()
    if save:
        plt.savefig(os.path.join(path, "{}_violations.pdf".format(network.lower())))
    else:
        plt.show()

    # for LVM plot invalid not-converged violations plot
    if network == "LVM":
        vio_idx = [5, 4, 3]
        labels = ["Transformer Overloadings", "Line Overloadings", "Voltage Deviations"]
        widths = [0.45, 0.45, 0.45]
        fig, ax = plt.subplots(figsize=(9, 3))
        bp = ax.boxplot(
            [violations[idx] for idx in vio_idx],
            vert=False,
            patch_artist=True,
            widths=widths,
            positions=None,
            labels=labels,
        )
        for patch in bp["boxes"]:
            patch.set_linewidth(1.25)
            patch.set_facecolor(colors["blue"])
            patch.set_alpha(0.75)

        for whisker in bp["whiskers"]:
            whisker.set(color="k", linewidth=1.75, linestyle="-")
            whisker.set_alpha(0.75)

        for cap in bp["caps"]:
            cap.set(color="k", linewidth=1.5)

        for median in bp["medians"]:
            median.set(color="k", linewidth=1.25)
            median.set_alpha(0.75)

        ax.set_xlabel("Maximum Violation in Percentage Points")
        plt.tight_layout()
        if save:
            plt.savefig(
                os.path.join(path, "{}_violations_2.pdf".format(network.lower()))
            )
        else:
            plt.show()


def _get_violations(df):
    # invalid / converged
    voltage_violations_invconv, line_violations_invconv, trafo_violations_invconv = (
        list(),
        list(),
        list(),
    )
    for l in df["voltage_violations_invconv"].to_numpy():
        l = [el for el in l if el != 0]
        voltage_violations_invconv.extend(l)
    for l in df["line_violations_invconv"].to_numpy():
        l = [el for el in l if el != 0]
        line_violations_invconv.extend(l)
    for l in df["trafo_violations_invconv"].to_numpy():
        l = [el for el in l if el != 0]
        trafo_violations_invconv.extend(l)

    # invalid / not converged
    voltage_violations_invnotc, line_violations_invnotc, trafo_violations_invnotc = (
        list(),
        list(),
        list(),
    )
    for l in df["voltage_violations_invnotc"].to_numpy():
        l = [el for el in l if el != 0]
        voltage_violations_invnotc.extend(l)
    for l in df["line_violations_invnotc"].to_numpy():
        l = [el for el in l if el != 0]
        line_violations_invnotc.extend(l)
    for l in df["trafo_violations_invnotc"].to_numpy():
        l = [el for el in l if el != 0]
        trafo_violations_invnotc.extend(l)

    return (
        voltage_violations_invconv,
        line_violations_invconv,
        trafo_violations_invconv,
        voltage_violations_invnotc,
        line_violations_invnotc,
        trafo_violations_invnotc,
    )


def violation_args():
    violation_args = {
        "LV": (
            [2],
            None,
            None,
            ["Transformer Overloadings"],
            (0, 3.5),
            None,
        ),
        "LVM": (
            [2, 0],
            [0.085, 0.095],
            [0.75, 1],
            ["Transformer Overloadings", "Voltage Deviations"],
            (-0.025, 0.525),
            (0.55, 1.2),
        ),
        "HV": (
            [0],
            None,
            None,
            ["Voltage Deviations"],
            (0, 0.25),
            None,
        ),
        "MV": (
            None,
            None,
            None,
            None,
            None,
            None,
        ),
    }
    return violation_args


def report_times(pkl_path, network):
    # prepare df
    with open(pkl_path, "rb") as file:
        metrics = pickle.load(file)
    df = _metrics_to_df(metrics)
    # eval ...
    t_ip, t_rl, speedup = (
        df.loc[:, ["ip_times", "rl_times", "speedup"]].mean().to_list()
    )
    print(f"\t========== {network} ==========")
    print(f"Times for {network} network ...")
    print(
        f"\t\tT_IP:\t{t_ip:.4f}\n"
        f"\t\tT_RL:\t{t_rl:.4f}\n"
        f"\t\tFac.:\t{speedup:.4f}"
    )


def report_constraint_satisfaction(pkl_path, network):
    # prepare df
    with open(pkl_path, "rb") as file:
        metrics = pickle.load(file)
    df = _metrics_to_df(metrics)
    # eval ...
    cons_train = (
        df.xs("training", level="mode")
        .loc[:, "valid_conv":"invalid_notconv"]
        .mean()
        .to_numpy()
    )
    cons_test = (
        df.xs("testing", level="mode")
        .loc[:, "valid_conv":"invalid_notconv"]
        .mean()
        .to_numpy()
    )
    print(f"Tables for {network} network ...")
    print("Training Data:")
    print(
        f"\t\t{cons_train[0]:.2f}\t {cons_train[2]:.2f}\n"
        f"\t\t {cons_train[1]:.2f}\t {cons_train[3]:.2f}"
    )
    print("Test Data:")
    print(
        f"\t\t{cons_test[0]:.2f}\t {cons_test[2]:.2f}\n"
        f"\t\t {cons_test[1]:.2f}\t {cons_test[3]:.2f}"
    )


def main(save=False, path=None):
    # activations(save, path)
    # capability_cost(save, path)
    distributions(save, path)

    # network specific args
    lc_args = learning_curve_args()
    vio_args = violation_args()

    # network specific plots and metrics
    for network in ["LV", "LVM", "HV", "MV"]:
        # plot learning curves
        csv_path = "/home/lukas/Schreibtisch/ma/rl/results/{}/rewards.csv".format(
            network
        )
        pkl_path = "/home/lukas/Schreibtisch/ma/rl/results/{0}/metrics_{0}.pkl".format(
            network
        )
        learning_curve(
            *lc_args[network],
            csv_path=csv_path,
            network=network,
            save=save,
            path=path,
        )
        # plot metrics
        metrics_plot(
            pkl_path=pkl_path,
            network=network,
            save=save,
            path=path,
        )
        # plot violations
        if network != "MV":
            violations(
                *vio_args[network],
                pkl_path=pkl_path,
                network=network,
                save=save,
                path=path,
            )
        # report info on times and constraint satisfaction
        report_times(
            pkl_path=pkl_path,
            network=network,
        )
        report_constraint_satisfaction(
            pkl_path=pkl_path,
            network=network,
        )

    # plot MVT metrics
    network = "MVT"
    pkl_path = "/home/lukas/Schreibtisch/ma/rl/results/{0}/metrics_{0}.pkl".format(
        network
    )
    metrics_plot(
        pkl_path=pkl_path,
        network=network,
        save=save,
        path=path,
    )


def set_font_sizes():
    """
    Sets the font sizes.
    """
    SMALL_SIZE = 9
    MEDIUM_SIZE = 11
    BIGGER_SIZE = 12
    plt.rc("font", size=SMALL_SIZE)  # controls default text sizes
    plt.rc("axes", titlesize=MEDIUM_SIZE)  # fontsize of the axes title
    plt.rc("axes", labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("legend", fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc("figure", titlesize=BIGGER_SIZE)  # fontsize of the figure title


def ggplot_colors():
    """
    Returns a ggplot colors dict.
    """
    colors = plt.style.library["ggplot"]["axes.prop_cycle"].by_key()["color"]
    colors = {
        "red": colors[0],
        "blue": colors[1],
        "purple": colors[2],
        "gray": colors[3],
        "yellow": colors[4],
        "green": colors[5],
        "rose": colors[6],
    }
    return colors


def seaborn_colors():
    """
    Returns a seaborn dark palette colors dict.
    """
    colors = plt.style.library["seaborn-dark-palette"]["axes.prop_cycle"].by_key()[
        "color"
    ]
    colors = {
        "blue": colors[0],
        "green": colors[1],
        "red": colors[2],
        "purple": colors[3],
        "yellow": colors[4],
        "aqua": colors[5],
    }
    return colors


if __name__ == "__main__":
    import argparse
    from distutils.util import strtobool

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-s",
        "--save",
        default=False,
        type=lambda x: bool(strtobool(x)),
        help="whether to save the plots or not",
    )
    parser.add_argument(
        "-p", "--path", default="./plots/", type=str, help="the save path"
    )

    args = vars(parser.parse_args())
    print(f"Running with args: {args}")

    main(save=args["save"], path=args["path"])
