import argparse
from collections import defaultdict
from distutils.util import strtobool
from multiprocessing import Pool, cpu_count
from pathlib import Path
import pickle
import os

import gym
import numpy as np
import tensorflow as tf

# suppress tf warnings ...
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
if type(tf.contrib) != type(tf):
    tf.contrib._warning = None

from stable_baselines import SAC
from stable_baselines.common.vec_env import DummyVecEnv, VecNormalize
from gym_powersystem.core.utils import Timer


def evaluate_agent(args):
    """"""
    network, agent_dir, mode, n_eval_episodes, seed = args
    model, env = _setup_agent(network, agent_dir, mode)

    # set seed after loading model!
    np.random.seed(seed)

    case_counter = dict.fromkeys(
        {"valid_conv", "valid_notconv", "invalid_conv", "invalid_notconv"}, 0
    )
    rl_rewards, opf_rewards = list(), list()
    rl_times, opf_times = list(), list()
    voltage_violations_invconv, line_violations_invconv, trafo_violations_invconv = (
        list(),
        list(),
        list(),
    )
    voltage_violations_invnotc, line_violations_invnotc, trafo_violations_invnotc = (
        list(),
        list(),
        list(),
    )
    obs = env.reset()
    for i in range(n_eval_episodes):
        with Timer() as rl_timer:
            act, _ = model.predict(obs, deterministic=True)
        # unpleasant unpacking ...
        [
            (rl_valid_sol, rl_violations, rl_reward, opf_is_conv, opf_reward, opf_timer)
        ] = env.env_method("eval_step", act.flatten())
        obs = env.reset()

        # four possible cases
        if rl_valid_sol and opf_is_conv:
            # append for metric evaluation
            rl_rewards.append(rl_reward)
            opf_rewards.append(opf_reward)
            rl_times.append(rl_timer.elapsed())
            opf_times.append(opf_timer.elapsed())
            case_counter["valid_conv"] += 1
        elif rl_valid_sol and not opf_is_conv:
            case_counter["valid_notconv"] += 1
        elif not rl_valid_sol and opf_is_conv:
            case_counter["invalid_conv"] += 1
            voltage_violations_invconv.append(rl_violations["max_voltage_deviation"])
            line_violations_invconv.append(rl_violations["max_line_overloading"])
            trafo_violations_invconv.append(rl_violations["max_trafo_overloading"])
        else:
            case_counter["invalid_notconv"] += 1
            voltage_violations_invnotc.append(rl_violations["max_voltage_deviation"])
            line_violations_invnotc.append(rl_violations["max_line_overloading"])
            trafo_violations_invnotc.append(rl_violations["max_trafo_overloading"])

    metrics = _evaluate_metrics(
        rl_rewards,
        opf_rewards,
        rl_times,
        opf_times,
        voltage_violations_invconv,
        line_violations_invconv,
        trafo_violations_invconv,
        voltage_violations_invnotc,
        line_violations_invnotc,
        trafo_violations_invnotc,
        case_counter,
    )
    return metrics


def _setup_agent(network, agent_dir, mode):
    load_path = os.path.join(agent_dir, "best_model")
    stats_path = load_path + "_stats.pkl"
    env = DummyVecEnv(
        [
            lambda: gym.make(
                id="PowerSystem-v0",
                network="MV" if network == "MVT" else network,
                testing=False if mode == "training" else True,
            )
        ]
    )
    env = VecNormalize.load(stats_path, env)
    env.training = False
    env.norm_reward = False
    model = SAC.load(load_path, env=env, verbose=1)
    return model, env


def _evaluate_metrics(
    rl_rewards,
    opf_rewards,
    rl_times,
    opf_times,
    voltage_violations_invconv,
    line_violations_invconv,
    trafo_violations_invconv,
    voltage_violations_invnotc,
    line_violations_invnotc,
    trafo_violations_invnotc,
    case_counter,
):
    # create arrays
    rl_rewards, opf_rewards, rl_times, opf_times = (
        np.array(rl_rewards),
        np.array(opf_rewards),
        np.array(rl_times),
        np.array(opf_times),
    )
    # mpe
    mpe = np.mean((rl_rewards / opf_rewards - 1) * 100)
    # medpe
    medpe = np.median((rl_rewards / opf_rewards - 1) * 100)
    # times
    opf_times_mean, rl_times_mean = np.mean(opf_times), np.mean(rl_times)
    speedup = opf_times_mean / rl_times_mean
    # counts
    total_count = sum(case_counter.values())
    for key, value in case_counter.items():
        case_counter[key] = value / total_count

    metrics = {
        "mpe": mpe,
        "medpe": medpe,
        "ip_times": opf_times_mean,
        "rl_times": rl_times_mean,
        "speedup": speedup,
        "voltage_violations_invconv": voltage_violations_invconv,
        "line_violations_invconv": line_violations_invconv,
        "trafo_violations_invconv": trafo_violations_invconv,
        "voltage_violations_invnotc": voltage_violations_invnotc,
        "line_violations_invnotc": line_violations_invnotc,
        "trafo_violations_invnotc": trafo_violations_invnotc,
    }
    metrics.update(case_counter)
    return metrics


def main(seed, network=None, n_episodes=None, save=False):
    """"""
    metrics = defaultdict(dict)
    log_dir = "/home/lukas/Schreibtisch/ma/rl/results/{}".format(network)
    modes = ["training", "testing"]
    agent_dirs = [f.path for f in os.scandir(log_dir) if f.is_dir()]

    args = [
        (network, agent_dir, mode, n_episodes, seed)
        for agent_dir in agent_dirs
        for mode in modes
    ]
    p = Pool()
    results = p.map(evaluate_agent, args)
    for i, result in enumerate(results):
        # results come in alternated mode order
        metrics[str(i // 2)][modes[i % 2]] = result
    # save to .pkl file
    if save:
        with open(
            os.path.join(log_dir, "metrics_{}.pkl".format(network)), "wb"
        ) as file:
            pickle.dump(metrics, file)


if __name__ == "__main__":
    # setup argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--seed", default=0, type=int)
    parser.add_argument("-n", "--network", default=None, type=str)
    parser.add_argument("-e", "--episodes", default=1000, type=int)
    parser.add_argument(
        "-sa", "--save", default=False, type=lambda x: bool(strtobool(x))
    )
    args = vars(parser.parse_args())
    print(f"Running with args: {args}")

    with Timer() as t:
        main(
            seed=args["seed"],
            network=args["network"],
            n_episodes=args["episodes"],
            save=args["save"],
        )
    print(t.elapsed())
