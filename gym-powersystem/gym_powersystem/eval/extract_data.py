import os
from pathlib import Path
import pandas as pd
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator


def tb_logs_to_csv(path=None, csv_name=None):
    """"""
    events = list()
    for event_path in Path(path).rglob("event*"):
        events.append(str(event_path.absolute()))
    accumulators = [EventAccumulator(event).Reload() for event in events]
    tag = accumulators[0].Tags()["scalars"][0]

    out = pd.DataFrame()
    for idx, acc in enumerate(accumulators):
        steps = [e.step for e in acc.Scalars(tag)]
        rewards = [e.value for e in acc.Scalars(tag)]
        if out.empty:
            out = pd.DataFrame(index=steps)
            out["rew_" + str(idx)] = rewards
        else:
            to_concat = pd.DataFrame(rewards, index=steps, columns=["rew_" + str(idx)])
            out = pd.concat([out, to_concat], axis=1)

    out.fillna(method="pad", axis=0, inplace=True)
    out = calculate_metrics(out)
    out.to_csv(os.path.join(path, csv_name), float_format="%.2f")


def concat_csvs(path=None, csv_name=None):
    csvs = list()
    for csv_path in Path(path).rglob("*csv"):
        csvs.append(str(csv_path.absolute()))

    dfs = [pd.read_csv(csv, names=["rew"]) for csv in csvs]
    df = pd.concat(dfs, axis=1)
    df = calculate_metrics(df)
    df.to_csv(os.path.join(path, csv_name), float_format="%.2f")


def calculate_metrics(df):
    """
    Calculate row-wise mean and std.
    """
    df["mean"], df["std"] = df.mean(axis=1), df.std(axis=1)
    return df


def main(path=None, csv_name=None):
    """"""
    # tb_logs_to_csv(path, csv_name)
    concat_csvs(path, csv_name)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--path",
        default=None,
        type=str,
        help="the directory to search for tensorboard/csv logs",
    )
    parser.add_argument(
        "-c",
        "--csv_name",
        default=None,
        type=str,
        help="the csv save name",
    )
    args = vars(parser.parse_args())
    print(f"Running with args: {args}")

    main(path=args["path"], csv_name=args["csv_name"])
