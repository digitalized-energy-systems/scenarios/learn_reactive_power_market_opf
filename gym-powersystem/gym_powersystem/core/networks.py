from abc import ABC, abstractmethod

import numpy as np
import pandas as pd

from pandapower import runpp, runopp, create_poly_cost
from pandapower.powerflow import LoadflowNotConverged
from pandapower.optimal_powerflow import OPFNotConverged
from simbench import get_absolute_values, get_simbench_net

from gym_powersystem.core.utils import Timer


class BaseNetwork(ABC):
    """Basic network class that provides a lot of functionality for derived networks.
    Networks that inherit from BaseNetwork have to define network_settings, scale_sgens
    and scale_loads."""

    def __init__(
        self,
        name,
        sb_code,
        cost_adjustment,
        voltage_penalty_factor,
        line_penalty_factor,
        trafo_penalty_factor,
    ):
        self.name = name
        self.sb_code = sb_code
        self.cost_adjustment = cost_adjustment
        self.voltage_penalty_factor = voltage_penalty_factor
        self.line_penalty_factor = line_penalty_factor
        self.trafo_penalty_factor = trafo_penalty_factor
        self.net = get_simbench_net(self.sb_code)
        self.n_sgens = None
        self.n_loads = None
        self.n_grids = None
        self.profiles = None
        self.n_timesteps = 35_136
        self.profiles = self._receive_profiles()

        # network specific settings
        self.network_settings()
        # set number of elements
        self.n_sgens, self.n_loads, self.n_grids = self._get_element_count()
        # receive generation and load profiles
        # set physical limits for sgens and loads from profiles
        self._limits_from_profiles()
        # initial powerflow
        self._powerflow()
        # assign cost
        self._assign_cost()

    @abstractmethod
    def network_settings(self):
        """Network-specific settings."""
        pass

    @abstractmethod
    def scale_sgens(self):
        """Randomly scale all static generators."""
        pass

    @abstractmethod
    def scale_loads(self):
        """Randomly scale all loads."""
        pass

    def _get_element_count(self):
        """Get number of static generators, loads and external grids."""
        n_sgens = len(self.net.sgen.index)
        n_loads = len(self.net.load.index)
        n_grids = len(self.net.ext_grid.index)
        return n_sgens, n_loads, n_grids

    def get_observation_len(self):
        """Get length of observation space."""
        return len(self.get_state())

    def get_action_len(self):
        """Get length of action space."""
        return self.n_sgens

    def take_action(self, action):
        """Calculate reactive power setpoints of static generators."""
        action = np.clip(action, -1, 1, dtype=np.float32)
        q_set = action * self.net.sgen["sn_mva"]
        q_max = self.net.sgen["q_max_mvar"]
        # save for visualization
        self.net.sgen["q_mvar"] = np.clip(q_set, -q_max, q_max, dtype=np.float32)
        # run powerflow
        self._powerflow()

    def _bus_loadings(self):
        """Assign buses aggregated power values for active and reactive power."""
        power_dict = {
            "p_sgen": (self.net.sgen, "p_mw", -1),
            "q_sgen": (self.net.sgen, "q_mvar", -1),
            "p_load": (self.net.load, "p_mw", 1),
            "q_load": (self.net.load, "q_mvar", 1),
        }
        # initialize with zeros
        self.net.bus["p_mw"], self.net.bus["q_mvar"] = 0.0, 0.0
        # calculate active and reactive power at each bus
        for type, unit, sign in power_dict.values():
            values = type.groupby(["bus"])[unit].agg("sum") * sign
            values = values.reindex(self.net.bus.index).fillna(0)
            self.net.bus[unit] += values

    def _maximum_reactive_power(self):
        self.net.sgen["q_max_mvar"] = (
            self.net.sgen["sn_mva"].pow(2) - self.net.sgen["p_mw"].pow(2)
        ).pow(0.5)

    def get_state(self):
        """Compute the environment state. State is defined by aggregated active and
        reactive power at every bus, as well as reactive power cost."""
        # bus loadings
        self._bus_loadings()
        p_bus = self.net.bus["p_mw"].to_numpy()
        q_bus = self.net.bus["q_mvar"].to_numpy()
        # maximum reactive power
        self._maximum_reactive_power()
        q_max = self.net.sgen["q_max_mvar"].to_numpy()
        # reactive power cost
        cost_sgen = self.net.sgen["cost"].to_numpy()
        cost_ext = self.net.ext_grid["cost"].to_numpy()
        # return concatenated vectors
        return np.concatenate((p_bus, q_bus, q_max, cost_sgen, cost_ext), axis=0)

    def get_reward(self):
        """
        Compute the agent's reward.
        """
        # reactive power cost
        cost = self._reactive_power_cost()
        # penalties for violations of bus voltages, line and trafo loadings
        penalty, violations = self._constraint_violations()
        return (cost + penalty), penalty, violations

    def _active_power_losses_cost(self):
        """Compute active power losses in the network."""
        losses = self.net.res_sgen["p_mw"].sum() + self.net.res_ext_grid["p_mw"].sum()
        return -losses

    def _reactive_power_cost(self):
        """Compute the reactive power cost."""
        # cost for reactive power provided by static generators
        sgen_reactive_power = self.net.res_sgen["q_mvar"]
        sgen_cost = self.net.sgen["cost"]
        total_sgen_cost = (sgen_reactive_power ** 2 * sgen_cost).sum()

        # cost for reactive power provided by external grid
        ext_grid_reactive_power = self.net.res_ext_grid["q_mvar"]
        ext_grid_cost = self.net.ext_grid["cost"]
        total_ext_grid_cost = (ext_grid_reactive_power ** 2 * ext_grid_cost).sum()

        return -(total_sgen_cost + total_ext_grid_cost) * self.cost_adjustment

    def _constraint_violations(self):
        """Compute the penalties for constraint violations."""
        # violations dict for evaluation
        violations = dict.fromkeys(
            ["max_voltage_deviation", "max_line_overloading", "max_trafo_overloading"]
        )

        # bus voltage deviations
        lower_bus_filt = self.net.bus["min_vm_pu"] - self.net.res_bus["vm_pu"] > 0
        lower_voltage_deviation = (
            self.net.bus.loc[lower_bus_filt, "min_vm_pu"]
            - self.net.res_bus.loc[lower_bus_filt, "vm_pu"]
        )

        upper_bus_filt = self.net.bus["max_vm_pu"] - self.net.res_bus["vm_pu"] < 0
        upper_voltage_deviation = (
            self.net.res_bus.loc[upper_bus_filt, "vm_pu"]
            - self.net.bus.loc[upper_bus_filt, "max_vm_pu"]
        )
        # add to violations dict
        violations["max_voltage_deviation"] = (
            max(0, lower_voltage_deviation.max(), upper_voltage_deviation.max()) * 100
        )
        # penalty
        voltage_deviation = (
            lower_voltage_deviation.sum() + upper_voltage_deviation.sum()
        )
        voltage_penalty = voltage_deviation * self.voltage_penalty_factor

        # line overloadings
        line_filt = (
            self.net.res_line["loading_percent"] > self.net.line["max_loading_percent"]
        )
        line_overloading = (
            self.net.res_line.loc[line_filt, "loading_percent"]
            - self.net.line.loc[line_filt, "max_loading_percent"]
        )
        # add to violations dict
        violations["max_line_overloading"] = max(0, line_overloading.max())
        # penalty
        line_penalty = line_overloading.sum() * self.line_penalty_factor

        # trafo overloading
        trafo_filt = (
            self.net.res_trafo["loading_percent"]
            > self.net.trafo["max_loading_percent"]
        )
        trafo_overloading = (
            self.net.res_trafo.loc[trafo_filt, "loading_percent"]
            - self.net.trafo.loc[trafo_filt, "max_loading_percent"]
        )
        # add to violations dict
        violations["max_trafo_overloading"] = max(0, trafo_overloading.max())
        # penalty
        trafo_penalty = trafo_overloading.sum() * self.trafo_penalty_factor

        # total penalty
        total_penalty = -(voltage_penalty + line_penalty + trafo_penalty)
        return total_penalty, violations

    def reset_network(self, testing):
        """Reset network to initial state."""
        # assign new cost
        self._assign_cost()
        # crucial: reset reactive power of static generators
        self.net.sgen["q_mvar"] = 0
        # scale sgens and loads for training or testing purposes.
        if not testing:
            self.scale_sgens()
            self.scale_loads()
        else:
            timestep = np.random.randint(0, self.n_timesteps)
            self._apply_profiles(timestep)

    def _assign_cost(self, low=0.25, high=1.0):
        """Assign reactive power cost to static generators and external grid."""
        cost = np.random.uniform(
            low=low, high=high, size=(self.n_sgens + self.n_grids,)
        )
        self.net.sgen["cost"] = cost[: self.n_sgens]
        self.net.ext_grid["cost"] = cost[-self.n_grids :]

    def _apply_profiles(self, timestep):
        """Apply generation and load profiles based on a given timestep."""
        for element, parameter in self.profiles.keys():
            key = (element, parameter)
            self.net[element].loc[:, parameter] = self.profiles[key].loc[timestep]

    def _receive_profiles(self):
        """Receive profiles and delete unused profiles."""
        profiles = get_absolute_values(self.net, profiles_instead_of_study_cases=True)
        del profiles[("gen", "p_mw")], profiles[("storage", "p_mw")]
        return profiles

    def _limits_from_profiles(self):
        """Assign limits on active and reactive power based on given profiles."""
        # sgen limits based on initial "p_mw" column
        self.net.sgen["max_profile_p_mw"] = self.net.sgen["p_mw"]
        # load limits based on profile limits
        self.net.load["min_profile_p_mw"] = self.profiles["load", "p_mw"].min()
        self.net.load["max_profile_p_mw"] = self.profiles["load", "p_mw"].max()
        self.net.load["min_profile_q_mvar"] = self.profiles["load", "q_mvar"].min()
        self.net.load["max_profile_q_mvar"] = self.profiles["load", "q_mvar"].max()

    def _powerflow(self):
        """Run powerflow on network."""
        try:
            runpp(self.net)
        except LoadflowNotConverged:
            print("Not Converged!")

    def optimal_powerflow(self):
        """Run optimal power flow on network."""
        # setup constraints
        self.net.sgen["controllable"] = True
        self.net.sgen["min_p_mw"] = self.net.sgen["p_mw"]
        self.net.sgen["max_p_mw"] = self.net.sgen["p_mw"]
        self.net.sgen["min_q_mvar"] = -self.net.sgen["q_max_mvar"]
        self.net.sgen["max_q_mvar"] = self.net.sgen["q_max_mvar"]
        self.net.ext_grid["min_p_mw"], self.net.ext_grid["min_q_mvar"] = (
            -np.inf,
            -np.inf,
        )
        self.net.ext_grid["max_p_mvar"], self.net.ext_grid["max_q_mvar"] = (
            np.inf,
            np.inf,
        )
        # setup cost
        for sgen in self.net.sgen.index:
            create_poly_cost(
                net=self.net,
                element=sgen,
                et="sgen",
                cp1_eur_per_mw=0,
                cq2_eur_per_mvar2=self.net.sgen.at[sgen, "cost"] * self.cost_adjustment,
            )
        for ext_grid in self.net.ext_grid.index:
            create_poly_cost(
                net=self.net,
                element=ext_grid,
                et="ext_grid",
                cp1_eur_per_mw=0,
                cq2_eur_per_mvar2=self.net.ext_grid.at[ext_grid, "cost"]
                * self.cost_adjustment,
            )
        # run opf
        is_conv = False
        try:
            with Timer() as opf_timer:
                runopp(net=self.net)
            is_conv = True
            return is_conv, -self.net.res_cost, opf_timer
        except OPFNotConverged:
            return is_conv, -np.inf, None


class LVNetwork(BaseNetwork):
    """LV Rural Network."""

    def __init__(
        self,
        name="LV",
        sb_code="1-LV-rural1--1-no_sw",
        cost_adjustment=100_000,
        voltage_penalty_factor=1000,
        line_penalty_factor=1,
        trafo_penalty_factor=1,
    ):
        super(LVNetwork, self).__init__(
            name,
            sb_code,
            cost_adjustment,
            voltage_penalty_factor,
            line_penalty_factor,
            trafo_penalty_factor,
        )

    def network_settings(self):
        """Do not consider storages and adjust names."""
        self.net.storage["in_service"] = False
        self.net.sgen["name"] = self.net.sgen.index.map(lambda x: "PV" + str(x + 1))
        self.net.ext_grid.at[0, "name"] = "Ext. Grid"

    def scale_sgens(self):
        """Randomly scale all static generators."""
        scaling_pv = np.clip(np.random.exponential(scale=0.15), 0, 0.6)
        self.net.sgen["p_mw"] = scaling_pv * self.net.sgen["max_profile_p_mw"]

    def scale_loads(self):
        """Randomly scale all loads."""
        scaling = np.random.uniform(low=0, high=1, size=(self.n_loads,))
        self.net.load["p_mw"] = (
            scaling
            * (self.net.load["max_profile_p_mw"] - self.net.load["min_profile_p_mw"])
            + self.net.load["min_profile_p_mw"]
        )
        self.net.load["q_mvar"] = (
            scaling
            * (
                self.net.load["max_profile_q_mvar"]
                - self.net.load["min_profile_q_mvar"]
            )
            + self.net.load["min_profile_q_mvar"]
        )


class LVNetworkModified(LVNetwork):
    """Modified LV Rural Network with tightended constraints."""

    def __init__(
        self,
        name="LVM",
        sb_code="1-LV-rural1--1-no_sw",
        cost_adjustment=100_000,
        voltage_penalty_factor=1000,
        line_penalty_factor=1,
        trafo_penalty_factor=1,
    ):
        super(LVNetwork, self).__init__(
            name,
            sb_code,
            cost_adjustment,
            voltage_penalty_factor,
            line_penalty_factor,
            trafo_penalty_factor,
        )

    def network_settings(self):
        """Do not consider storages and adjust names."""
        self.net.storage["in_service"] = False
        self.net.sgen["name"] = self.net.sgen.index.map(lambda x: "PV" + str(x + 1))
        self.net.ext_grid.at[0, "name"] = "Ext. Grid"

        # tighten voltage limits, reduce max current and retain trafo size
        self.net.bus["min_vm_pu"], self.net.bus["max_vm_pu"] = 0.975, 1.025
        self.net.line["max_i_ka"] *= 0.33
        self.net.trafo["sn_mva"] *= 1


class MVNetwork(BaseNetwork):
    """MV Network."""

    def __init__(
        self,
        name="MV",
        sb_code="1-MV-urban--0-no_sw",
        cost_adjustment=10,
        voltage_penalty_factor=1000,
        line_penalty_factor=1,
        trafo_penalty_factor=1,
    ):
        super(MVNetwork, self).__init__(
            name,
            sb_code,
            cost_adjustment,
            voltage_penalty_factor,
            line_penalty_factor,
            trafo_penalty_factor,
        )

    def network_settings(self):
        """Initialize column and adjust names."""
        self.net.sgen["name"] = self.net.sgen.index.map(lambda x: "PV" + str(x + 1))
        self.net.sgen.at[133, "name"] = "Hydro"
        self.net.ext_grid.at[0, "name"] = "Ext. Grid"

    def scale_sgens(self):
        """Randomly scale all static generators."""
        scaling_pv = np.clip(np.random.exponential(scale=0.075), 0, 0.6)
        self.net.sgen["p_mw"] = scaling_pv * self.net.sgen["max_profile_p_mw"]

        scaling_hydro = np.clip(np.random.exponential(scale=0.25), 0, 1)
        self.net.sgen.at[133, "p_mw"] = (
            scaling_hydro * self.net.sgen.at[133, "max_profile_p_mw"]
        )

    def scale_loads(self):
        """Randomly scale all loads."""
        scaling = np.random.uniform(low=0, high=1, size=(self.n_loads,))
        self.net.load["p_mw"] = (
            scaling
            * (self.net.load["max_profile_p_mw"] - self.net.load["min_profile_p_mw"])
            + self.net.load["min_profile_p_mw"]
        )
        self.net.load["q_mvar"] = (
            scaling
            * (
                self.net.load["max_profile_q_mvar"]
                - self.net.load["min_profile_q_mvar"]
            )
            + self.net.load["min_profile_q_mvar"]
        )


class HVNetwork(BaseNetwork):
    """HV Urban Network."""

    def __init__(
        self,
        name="HV",
        sb_code="1-HV-urban--0-no_sw",
        cost_adjustment=0.05,
        voltage_penalty_factor=1000,
        line_penalty_factor=1,
        trafo_penalty_factor=1,
    ):
        super(HVNetwork, self).__init__(
            name,
            sb_code,
            cost_adjustment,
            voltage_penalty_factor,
            line_penalty_factor,
            trafo_penalty_factor,
        )

    def network_settings(self):
        """Remove unused static generators and adjust names."""
        self.net.sgen = self.net.sgen.iloc[56:]
        self.net.sgen.loc[(self.net.sgen["type"] == "Wind"), "type"] = "WT"
        self.net.sgen["name"] = self.net.sgen.type + (self.net.sgen.index - 55).astype(
            "str"
        )
        wts = self.net.sgen["type"] == "WT"
        self.net.sgen.loc[wts, "name"] = self.net.sgen.loc[wts].type + (
            self.net.sgen.loc[wts].index - 78
        ).astype("str")
        self.net.ext_grid.at[0, "name"] = "Ext. Grid"

    def scale_sgens(self):
        """Randomly scale all static generators."""
        scaling_pv = np.clip(np.random.lognormal(mean=-1.3, sigma=0.39), 0, 1)
        self.net.sgen.loc[:78, "p_mw"] = (
            scaling_pv * self.net.sgen.loc[:78, "max_profile_p_mw"]
        )
        scaling_wind = np.clip(np.random.exponential(scale=0.175), 0, 1)
        self.net.sgen.loc[79:, "p_mw"] = (
            scaling_wind * self.net.sgen.loc[79:, "max_profile_p_mw"]
        )

    def scale_loads(self):
        """Randomly scale all loads."""
        scaling = np.random.uniform(low=0, high=1, size=(self.n_loads,))
        self.net.load["p_mw"] = (
            scaling
            * (self.net.load["max_profile_p_mw"] - self.net.load["min_profile_p_mw"])
            + self.net.load["min_profile_p_mw"]
        )
        self.net.load["q_mvar"] = (
            scaling
            * (
                self.net.load["max_profile_q_mvar"]
                - self.net.load["min_profile_q_mvar"]
            )
            + self.net.load["min_profile_q_mvar"]
        )
