import os
from timeit import default_timer
import yaml


class Timer:
    def __init__(self):
        self.timer = default_timer

    def __enter__(self):
        self.start = self.timer()
        return self

    def __exit__(self, *args):
        self.end = self.timer()

    def elapsed(self):
        return self.end - self.start


def load_config(current_file):
    parent_dir = os.path.abspath(os.path.dirname(current_file))
    config_path = os.path.join(parent_dir, "config/config.yml")
    with open(config_path, "r") as config_file:
        try:
            config = yaml.safe_load(config_file)
        except YAMLError as e:
            print(e)
    return config


def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, "__dict__"):
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, "__iter__") and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size
